# Computationalphotography lab 1

Code snippets from
- [Github Martin-Marek](https://github.com/martin-marek/hdr-plus-pytorch/) (see also [Google Colab](https://colab.research.google.com/github/martin-marek/hdr-plus-pytorch/blob/main/demo.ipynb))